USER=dshaff
PROJECT=psrchive
PROC_ARGS := --build-arg MAKE_PROC=$(shell nproc)
COMMIT=master
DOCKER_ARGS=
BUILD_VERSION := $(shell cat VERSION)

build:
	docker build $(DOCKER_ARGS) $(PROC_ARGS) -f Dockerfile.build -t $(USER)/$(PROJECT)-build:latest .

deploy:
	docker build $(DOCKER_ARGS) $(PROC_ARGS) --build-arg COMMIT=$(COMMIT) -f Dockerfile -t $(USER)/$(PROJECT):latest .

tag-build:
	docker tag $(USER)/$(PROJECT)-build:latest $(USER)/$(PROJECT)-build:$(BUILD_VERSION)

tag-deploy:
	docker tag $(USER)/$(PROJECT):latest $(USER)/$(PROJECT):$(shell /bin/sh psrchive-version.sh)

all: build tag-build deploy
