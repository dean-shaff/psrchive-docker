#! /bin/sh
VERSION=$(docker run dshaff/psrchive:latest /bin/sh -c "cd /home/psr/sources/psrchive && git rev-parse --short HEAD")
echo "${VERSION}"
