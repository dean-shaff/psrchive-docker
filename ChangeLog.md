## v0.2.0

- we now set the mandatory `TEMPO2` environment variable.
- no longer using CPPFLAGS in psrchive build
- Separated build and deploy dockerfiles. Dockerfile.build is for creating an environment in which to build psrchive. Dockerfile is for building psrchive

## v0.3.0

- Using cuda image instead of ubuntu image for psrchive-build
